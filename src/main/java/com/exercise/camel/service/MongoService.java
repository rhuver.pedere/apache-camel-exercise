package com.exercise.camel.service;
import com.exercise.camel.model.mongodb.Exercise;

public interface MongoService {
    void save(Exercise exercise);
    Exercise findByName(String name);
}
