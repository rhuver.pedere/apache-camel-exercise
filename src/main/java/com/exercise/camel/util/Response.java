package com.exercise.camel.util;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import java.io.IOException;

public final class Response {
    private static final String SUCCESS = "OK";
    private static final String ERROR = "Bad Request";
    private static final int SUCCESS_CODE = 200;
    private static final int ERROR_CODE = 400;

    public static final String createResponse(String response) throws IOException {
        Gson gson = new Gson();
        JsonObject jsonObject = new JsonObject();
        String toSend;

        if (response.equalsIgnoreCase(SUCCESS)) {
            jsonObject.addProperty("code", SUCCESS_CODE);
            jsonObject.addProperty("description", SUCCESS);
        } else {
            jsonObject.addProperty("code", ERROR_CODE);
            jsonObject.addProperty("description", ERROR);
        }
        toSend = gson.toJson(jsonObject);
        JsonObject jsonText = JsonParser.parseString(toSend).getAsJsonObject();
        return String.valueOf(jsonText);
    }
}
