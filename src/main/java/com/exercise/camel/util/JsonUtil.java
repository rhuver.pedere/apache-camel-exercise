package com.exercise.camel.util;

import com.google.gson.Gson;

public final class JsonUtil {
    public static String toJSONString(Object obj) {
        return new Gson().toJson(obj);
    }
    public static Object toObject(String jsonString, Class baseClass) {
        return new Gson().fromJson(jsonString, baseClass);
    }
}
