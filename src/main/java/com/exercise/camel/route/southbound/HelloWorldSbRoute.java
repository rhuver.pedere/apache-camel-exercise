package com.exercise.camel.route.southbound;

import org.apache.camel.Exchange;
import org.apache.camel.LoggingLevel;
import org.apache.camel.builder.RouteBuilder;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Component;

@Component
public class HelloWorldSbRoute extends RouteBuilder {

    public static final String DIRECT_HW = "direct:helloworld";
    private static final String BRIDGE = "?bridgeEndpoint=true&throwExceptionOnFailure=false";

    @Value("${config.endpoint.sb.helloworld}")
    private String sbHelloWorld;

    @Override
    public void configure() throws Exception {
        from(DIRECT_HW)
                .log(LoggingLevel.INFO, "Processing: ${body}, Processed by:${threadName}")
                .setHeader(Exchange.HTTP_METHOD, constant(HttpMethod.GET.toString()))
                .streamCaching()
                .to(sbHelloWorld + BRIDGE)
                .log(LoggingLevel.INFO, "Southbound Response:\n ${body}")
                .stop();
    }
}
