package com.exercise.camel.route.southbound;
import com.exercise.camel.processor.mongodb.GetProcessor;
import com.exercise.camel.processor.mongodb.PostProcessor;
import org.apache.camel.builder.RouteBuilder;
import org.springframework.stereotype.Component;

@Component
public class MongoDBSbRoute extends RouteBuilder {

    public static final String DIRECT_GET = "direct:get";
    public static final String DIRECT_POST = "direct:post";

    private final GetProcessor getProcessor;
    private final PostProcessor postProcessor;

    public MongoDBSbRoute(GetProcessor getProcessor, PostProcessor postProcessor) {
        this.getProcessor = getProcessor;
        this.postProcessor = postProcessor;
    }

    @Override
    public void configure() throws Exception {
        from(DIRECT_GET)
                .process(getProcessor);

        from(DIRECT_POST)
                .process(postProcessor);
    }
}
