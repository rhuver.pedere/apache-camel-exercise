package com.exercise.camel.route.northbound;

import com.exercise.camel.route.southbound.HelloWorldSbRoute;
import org.apache.camel.builder.RouteBuilder;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import javax.ws.rs.core.MediaType;

@Component
public class HelloWorldNbRoute extends RouteBuilder {

    @Value("${config.component.servlet}")
    private String servlet;

    @Value("${config.endpoint.nb.helloworld}")
    private String nbHelloWorld;

    @Override
    public void configure() throws Exception {
        routeDefinition();
    }

    private void routeDefinition() {
        restConfiguration().component(servlet);

        //Define REST endpoint details
        rest()
                //Create a REST Endpoint with HTTP Method GET
                .get(nbHelloWorld)

                //Specify resource representation
                .produces(MediaType.APPLICATION_JSON)

                //Start route definition
                .route()

                .to(HelloWorldSbRoute.DIRECT_HW)

                //End REST route definition
                .endRest();
    }
}
