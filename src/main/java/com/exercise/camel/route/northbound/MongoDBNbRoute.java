package com.exercise.camel.route.northbound;

import com.exercise.camel.route.southbound.MongoDBSbRoute;
import org.apache.camel.builder.RouteBuilder;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import javax.ws.rs.core.MediaType;

@Component
public class MongoDBNbRoute extends RouteBuilder {

    @Value("${config.component.servlet}")
    private String servlet;

    @Value("${config.endpoint.nb.mongodb}")
    private String nbMongoDb;

    @Override
    public void configure() throws Exception {
        routeDefinition();
    }

    private void routeDefinition() {
        restConfiguration().component(servlet);

        //Define REST endpoint details
        rest()
                //Create a REST Endpoint with HTTP Method GET
                .get(nbMongoDb)
                //Specify resource representation
                .produces(MediaType.APPLICATION_JSON)
                //Start route definition
                .route()
                .to(MongoDBSbRoute.DIRECT_GET)
                //End REST route definition
                .endRest();

        rest()
                //Create a REST Endpoint with HTTP Method POST
                .post(nbMongoDb)
                //Specify resource representation
                .produces(MediaType.APPLICATION_JSON)
                //Start route definition
                .route()
                .to(MongoDBSbRoute.DIRECT_POST)

                //End REST route definition
                .endRest();
    }
}
