package com.exercise.camel.processor.mongodb;

import com.exercise.camel.model.mongodb.Exercise;
import com.exercise.camel.service.MongoService;
import com.exercise.camel.util.JsonUtil;
import com.exercise.camel.util.Response;
import io.netty.handler.codec.http.HttpResponseStatus;
import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.springframework.stereotype.Component;

import static javax.ws.rs.core.MediaType.APPLICATION_JSON;

@Component
public class GetProcessor implements Processor {
    private static final String ERROR = "Bad Request";

    private final MongoService mongoService;

    public GetProcessor(MongoService mongoService) {
        this.mongoService = mongoService;
    }

    @Override
    public void process(Exchange exchange) throws Exception {

        String name = (String) exchange.getIn().getHeader("name");

        Exercise record = mongoService.findByName(name);

        if (record == null || record.getId() == null || "".equals(record.getId())) {
            exchange.getMessage().setBody(Response.createResponse(ERROR));
        } else {
            exchange.getMessage().setBody(JsonUtil.toJSONString(record));
            exchange.getMessage().setHeader(Exchange.HTTP_RESPONSE_CODE, HttpResponseStatus.OK.code());
            exchange.getMessage().setHeader(Exchange.CONTENT_TYPE, APPLICATION_JSON);
        }

    }
}
