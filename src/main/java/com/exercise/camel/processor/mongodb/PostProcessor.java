package com.exercise.camel.processor.mongodb;
import com.exercise.camel.model.mongodb.Exercise;
import com.exercise.camel.service.MongoService;
import com.exercise.camel.util.JsonUtil;
import com.exercise.camel.util.Response;
import io.netty.handler.codec.http.HttpResponseStatus;
import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import static javax.ws.rs.core.MediaType.APPLICATION_JSON;

@Component
public class PostProcessor implements Processor {
    private static final String SUCCESS = "OK";
    private static final String ERROR = "Bad Request";

    private final Logger logger = LoggerFactory.getLogger(this.getClass());

    private final MongoService mongoService;

    public PostProcessor(MongoService mongoService) {
        this.mongoService = mongoService;
    }

    @Override
    public void process(Exchange exchange) throws Exception {
        Object request;

        try {
            String jsonPayload = exchange.getIn().getBody(String.class);
            request = JsonUtil.toObject(jsonPayload, Exercise.class);

            String name = ((Exercise) request).getName();

            logger.info("NAME: {}", name);

            Exercise data = new Exercise();
            data.setName(name);
            mongoService.save(data);

            exchange.getMessage().setBody(Response.createResponse(SUCCESS));
            exchange.getMessage().setHeader(Exchange.HTTP_RESPONSE_CODE, HttpResponseStatus.OK.code());
            exchange.getMessage().setHeader(Exchange.CONTENT_TYPE, APPLICATION_JSON);

        } catch (NullPointerException e) {
            String body = exchange.getIn().getBody(String.class);
            logger.info("EXCEPTION: {}", e);
            logger.info("RESPONSE BODY: {}", body);
            exchange.getMessage().setBody(Response.createResponse(ERROR));
        }
    }
}
