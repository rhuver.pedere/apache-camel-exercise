package com.exercise.camel.dao;
import com.exercise.camel.model.mongodb.Exercise;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface MongoDao extends MongoRepository<Exercise, String> {

    /**
     * @param name
     * @return Exercise.class
     */
    Exercise findByName(String name);

}
