package com.exercise.camel.impl;

import com.exercise.camel.dao.MongoDao;
import com.exercise.camel.model.mongodb.Exercise;
import com.exercise.camel.service.MongoService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

@Service
public class MongoServiceImpl implements MongoService {

    private final Logger logger = LoggerFactory.getLogger(this.getClass());
    private final MongoDao mongoDao;

    public MongoServiceImpl(MongoDao mongoDao) {
        this.mongoDao = mongoDao;
    }

    @Override
    public void save(Exercise exercise) {
        Exercise record = mongoDao.findByName(exercise.getName());

        if (record == null || record.getId() == null || "".equals(record.getId())) {
            mongoDao.save(exercise);
        } else {
            logger.info("Name: {}, is already existing:", exercise.getName());
        }
    }

    @Override
    public Exercise findByName(String name) {
        return mongoDao.findByName(name);
    }
}
